/*
** TailwindCSS Configuration File
**
** Docs: https://tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
module.exports = {
  theme: {
    colors: {
      transparent: 'transparent',

      black: '#000',
      white: '#fff',

      brand: {
        'black': '#59595B',
        'red': '#D8232A',
      },
      data: {
        'blue': '#3389FE',
        'green': '#219653',
        'orange': '#F2994A',
        'purple': '#9B51E0',
        'red': '#EB5757',
        'yellow': '#F2C94C',
        'grey': '#1D2125',
      },
      social: {
        'facebook': '#4267B2',
        'instagram': '#C13584',
        'telegram': '#0088CC',
        'twitter': '#1DA1F2',
        'whatsapp': '#25D366',
      },
      ui: {
        100: '#F1F2F3',
        200: '#E1E2E6',
        300: '#B8BCC6',
        400: '#858A93',
        500: '#666B73',
        600: '#3C4249',
        700: '#2E343B',
        800: '#282D33',
        900: '#22272C',
      },
    },
    fontFamily: {
      sans: [
        '"IBM Plex Sans"',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
      ],
    },
  },
  variants: {},
  plugins: [
    require('@tailwindcss/custom-forms'),
  ],
};
