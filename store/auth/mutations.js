export default {
  setToken(state, payload) {
    state.token = payload;
  },

  setIsLoading(state, payload) {
    state.isLoading = payload;
  },

  setErrorEmail(state, payload) {
    state.errorEmail = payload;
  },

  setErrorPassword(state, payload) {
    state.errorPassword = payload;
  }
}
