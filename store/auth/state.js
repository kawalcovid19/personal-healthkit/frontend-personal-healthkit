export default () => ({
  token: '',

  isLoading: false,
  errorEmail: '',
  errorPassword: '',
  errorName: '',
  errorTel: '',
  errorAddress: '',
  errorCity: '',
});
