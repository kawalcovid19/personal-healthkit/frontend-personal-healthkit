export default {
  async login({ commit }, payload) {
    if (!payload) {
      return;
    }

    commit('setIsLoading', true);

    try {
      const res = await this.$axios.$post('login', payload);
      commit('setToken', res.result.token);
    } catch (err) {
      // commit error
    } finally {
      commit('setIsLoading', false);
    }
  }  
};
