export default {
  setError(state, payload) {
    state.error = payload
  },
  setIsLoading(state, payload) {
    state.is_loading = payload
  },
  setItem(state, payload) {
    state.item = payload
  },
  setRawData(state, payload) {
    state.rawData = payload
  },
}
