export default {
  items(state) {
    if (!state.rawData) return
    return state.rawData.data
  }
}
