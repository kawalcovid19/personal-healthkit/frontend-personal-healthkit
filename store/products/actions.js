export default {
  async fetchSingle({commit, state}, payload) {
    if (!payload) return
    try {
      const res = await this.$axios.$get(`products/${payload.id}`)
      if (res) commit('setItem', res)
    } catch (error) {
      commit('setError', error)
    }

  },
  async fetchAll({commit}) {
    try {
      const res = await this.$axios.$get('products')
      if (res) commit('setRawData', res)
      commit('setIsLoading', false)
    } catch (error) {
      commit('setError', error)
      commit('setIsLoading', false)
    }
  }
}
