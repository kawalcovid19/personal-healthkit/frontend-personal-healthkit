export default () => ({
  isLoading: false,

  errorEmail: '',
  errorPassword: '',
  errorName: '',
  errorTel: '',
  errorAddress: '',
  errorCity: '',
});
