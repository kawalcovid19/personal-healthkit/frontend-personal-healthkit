export default {
  async register({ commit }, payload) {
    if (!payload) {
      return;
    }

    commit('setIsLoading', true);

    try {
      await this.$axios.$post('register', payload);
    } catch (err) {
      // set error
    } finally {
      commit('setIsLoading', false);
    }
  },
};
