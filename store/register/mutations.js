export default {
  setIsLoading(state, payload) {
    state.isLoading = payload;
  },

  setErrorEmail(state, payload) {
    state.errorEmail = payload;
  },

  setErrorPassword(state, payload) {
    state.errorPassword = payload;
  },
  
  setErrorName(state, payload) {
    state.errorName = payload;
  },

  setErrorTel(state, payload) {
    state.errorTel = payload;
  },

  setErrorAddress(state, payload) {
    state.errorAddress = payload;
  },

  setErrorCity(state, payload) {
    state.errorCity = payload;
  },
};
